import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './pages/Home/Home';
import Login from './pages/Login/Login';
import Register from './pages/Register/Register';
import DaftarJual from './pages/DaftarJual/DaftarJual';
import DetailProduk from './pages/DetailProduk/DetailProduk';

const RouteApp = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/register" element={<Register />} />
                <Route path="/login" element={<Login />} />
                <Route path="/daftarjual" element={<DaftarJual />} />
                <Route path="/detailproduk" element={<DetailProduk />} />
            </Routes>
        </BrowserRouter>
    );
};

export default RouteApp;
